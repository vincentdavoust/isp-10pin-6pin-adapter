EESchema Schematic File Version 2  date Mon Dec  2 21:06:29 2013
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ISP-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "2 dec 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_5X2 P10
U 1 1 529CE565
P 6700 2600
F 0 "P10" H 6700 2900 60  0000 C CNN
F 1 "CONN_5X2" V 6700 2600 50  0000 C CNN
F 2 "~" H 6700 2600 60  0000 C CNN
F 3 "~" H 6700 2600 60  0000 C CNN
	1    6700 2600
	1    0    0    -1  
$EndComp
$Comp
L CONN_3X2 P6
U 1 1 529CE574
P 8150 2600
F 0 "P6" H 8150 2850 50  0000 C CNN
F 1 "CONN_3X2" V 8150 2650 40  0000 C CNN
F 2 "~" H 8150 2600 60  0000 C CNN
F 3 "~" H 8150 2600 60  0000 C CNN
	1    8150 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2450 7500 2450
Wire Wire Line
	7500 2450 7500 3050
Wire Wire Line
	7500 3050 6300 3050
Wire Wire Line
	6300 3050 6300 2800
Wire Wire Line
	7100 2200 7100 2400
Wire Wire Line
	7100 2200 8550 2200
Wire Wire Line
	8550 2200 8550 2450
Wire Wire Line
	7100 2500 7100 2850
Wire Wire Line
	7750 2550 7550 2550
Wire Wire Line
	7550 2550 7550 3100
Wire Wire Line
	7550 3100 6200 3100
Wire Wire Line
	6200 3100 6200 2700
Wire Wire Line
	6200 2700 6300 2700
Wire Wire Line
	7750 2650 7600 2650
Wire Wire Line
	7600 2650 7600 3150
Wire Wire Line
	7600 3150 6150 3150
Wire Wire Line
	6150 3150 6150 2600
Wire Wire Line
	6150 2600 6300 2600
Wire Wire Line
	8550 2550 8600 2550
Wire Wire Line
	8600 2550 8600 2150
Wire Wire Line
	8600 2150 6300 2150
Wire Wire Line
	6300 2150 6300 2400
Wire Wire Line
	8550 2650 8550 2750
Connection ~ 7100 2800
$Comp
L GND #PWR01
U 1 1 529CE610
P 7100 2850
F 0 "#PWR01" H 7100 2850 30  0001 C CNN
F 1 "GND" H 7100 2780 30  0001 C CNN
F 2 "" H 7100 2850 60  0000 C CNN
F 3 "" H 7100 2850 60  0000 C CNN
	1    7100 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 529CE61D
P 8550 2750
F 0 "#PWR02" H 8550 2750 30  0001 C CNN
F 1 "GND" H 8550 2680 30  0001 C CNN
F 2 "" H 8550 2750 60  0000 C CNN
F 3 "" H 8550 2750 60  0000 C CNN
	1    8550 2750
	1    0    0    -1  
$EndComp
Connection ~ 8550 2750
Connection ~ 7100 2850
Connection ~ 7100 2500
Connection ~ 7100 2600
Connection ~ 7100 2700
Wire Wire Line
	7100 2500 6300 2500
$EndSCHEMATC
